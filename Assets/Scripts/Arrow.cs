﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

    public PlayerScript player;
    public Ball ball;
    private SpriteRenderer spriteRenderer;
    private float speedDivider = 15.0f;
    private float preferredDistance = 50.0f;
    private float engagementDistance = 300.0f;
    private bool locked = false;
    private float unlockedTimer = 0.0f;
    private float timeToLock = 100.0f;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if ( !ball.inGoal ) {
            // Color
            spriteRenderer.color = ball.color;
            // Position
            Vector3 distance = ( ball.transform.position - player.transform.position );
            Vector3 targetPosition;
            if ( distance.magnitude < engagementDistance ) {
                locked = false;
                unlockedTimer = 1;
                targetPosition = ball.transform.position - distance.normalized * preferredDistance;
                if ( ( targetPosition - transform.position ).magnitude >= distance.magnitude - preferredDistance * 2.0f ) {
                    targetPosition = ( ball.transform.position + player.transform.position ) / 2.0f;
                }
                Vector3 movementDirection = targetPosition - transform.position;
                transform.position = transform.position + ( movementDirection / speedDivider );
            } else {
                targetPosition = player.transform.position + distance.normalized * preferredDistance;
                if ( locked || unlockedTimer == timeToLock ) {
                    transform.position = targetPosition;
                } else {
                    Vector3 movementDirection = targetPosition - transform.position;
                    transform.position = transform.position + movementDirection * ( unlockedTimer / timeToLock );
                    unlockedTimer++;
                }
            }
            // Direction
            Vector3 facingDirection = ball.transform.position - transform.position;
            float direction = -Mathf.Atan2( facingDirection.normalized.x, facingDirection.normalized.y ) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3( 0.0f, 0.0f, direction );
        } else {
            // Deactivate
            gameObject.SetActive( false );
        }
	}
}
