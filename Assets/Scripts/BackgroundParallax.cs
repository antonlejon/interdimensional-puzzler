﻿using UnityEngine;
using System.Collections;

public class BackgroundParallax : MonoBehaviour {

    public Camera cameraToFollow;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector2( cameraToFollow.transform.position.x * 0.9f, cameraToFollow.transform.position.y * 0.9f );
	}
}
