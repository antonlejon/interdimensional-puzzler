﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    public bool inGoal = false;
    public int ballId = 1;
    public Color32 color;
    public bool overrideSpriteColor = true;
    public bool overrideLightColor = true;
    public bool overrideParticleSystemColor = true;

	// Use this for initialization
	void Start () {
        if ( overrideLightColor ) {
            Transform lightTransform = transform.FindChild( "Point light" );
            Light pointLight = lightTransform.GetComponent<Light>();
            pointLight.color = color;
        }
        if ( overrideSpriteColor ) {
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.color = color;
        }
        if ( overrideParticleSystemColor ) {
            Transform particleSystemTransform = transform.FindChild( "Particle System" );
            ParticleSystem particleSystem = particleSystemTransform.GetComponent<ParticleSystem>();
            particleSystem.startColor = color;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ReachedGoal() {
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        foreach ( GameObject obj in objects ) {
            if ( obj.CompareTag( this.gameObject.tag ) ) {
                if ( obj != this.gameObject ) {
                    Ball ball = obj.GetComponent( "Ball" ) as Ball;
                    ball.Nirvana();
                }
            }
        }
        Nirvana();
    }

    public void Nirvana() {
        StartCoroutine( KillOnAnimationEnd() );
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if ( rb.velocity.magnitude > 20.0f ) {
            rb.velocity = rb.velocity.normalized * 20.0f;
        }
    }

    private IEnumerator KillOnAnimationEnd() {
        Animator animator = this.gameObject.GetComponent( "Animator" ) as Animator;
        animator.Play( "Ball death" );
        yield return new WaitForSeconds( 6.0f );
        gameObject.active = false;
        inGoal = true;
        GlobalVariables globalVariables = GameObject.Find( "Overseer" ).GetComponent( "GlobalVariables" ) as GlobalVariables;
        globalVariables.ballReachedGoal( ballId );
    }
}
