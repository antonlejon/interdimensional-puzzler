﻿using UnityEngine;
using System.Collections;

public class BallGoal : MonoBehaviour {

    public string objectTag;
    public float gravity;
    public float gravitationalReach;
    public Ball[] balls;

	// Use this for initialization
	void Start () {
        balls = FindObjectsOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
        //this.transform.Rotate( new Vector3( 0.0f, 0.5f, 0.5f ) );
        foreach ( Ball ball in balls ) {
            Vector2 distance = transform.position - ball.transform.position;
            if ( distance.magnitude < gravitationalReach ) {
                PullBall( distance.normalized * gravity, ball.ballId );
            }
        }
	}

    private void PullBall( Vector2 force, int ballId ) {
        foreach ( Ball ball in balls ) {
            if ( ball.ballId == ballId ) {
                Rigidbody2D ballBody = ball.gameObject.GetComponent<Rigidbody2D>();
                ballBody.AddForce( force );
            }
        }
    }

    void OnTriggerEnter2D( Collider2D collider ) {
        if ( collider.gameObject.CompareTag( objectTag ) ) {
            Ball ball = collider.gameObject.GetComponent( "Ball" ) as Ball;
            ball.ReachedGoal();
            StartCoroutine( KillOnAnimationEnd() );
        }
    }

    private IEnumerator KillOnAnimationEnd() {
        Animator animator = this.gameObject.GetComponent( "Animator" ) as Animator;
        animator.Play( "Death" );
        yield return new WaitForSeconds( 27.0f );
        Destroy( gameObject );
        Debug.Log( "2" );
    }

    void OnDrawGizmos() {
        Gizmos.color = new Color32( 255, 255, 0, 65 );
        Gizmos.DrawSphere( transform.position, gravitationalReach );
    }
}
