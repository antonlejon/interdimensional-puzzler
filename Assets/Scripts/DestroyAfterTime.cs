﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {
    
    public float lifetime = 2.5f;
    private float createdAtTime;

	// Use this for initialization
	void Start () {
        this.createdAtTime = Time.fixedTime;
	}
	
	// Update is called once per frame
	void Update () {
        if ( Time.fixedTime >= this.createdAtTime + lifetime ) {
            Destroy( this.gameObject );
        }
	}
}
