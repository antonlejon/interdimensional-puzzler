﻿using UnityEngine;
using System.Collections;

public class Friction : MonoBehaviour {

    public float frictionMultiplier = 4.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Rigidbody2D>().AddForce( -this.GetComponent<Rigidbody2D>().velocity * frictionMultiplier );
	}
}
