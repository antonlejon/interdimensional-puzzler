﻿using UnityEngine;
using System.Collections;

public class GlobalVariables : MonoBehaviour {

    public int numberOfPlayers = 2;
    public int numberOfPlayersInGoal = 0;
    public int numberOfPrizes = 0;
    public int numberOfPrizesInGoal = 0;
    private bool countdownHasStarted;
    private float endTime;
    public Font font;

    public bool[] ballInGoal;

	// Use this for initialization
	void Start () {
	    
    }
	
	// Update is called once per frame
	void Update () {
        if ( /*numberOfPlayersInGoal >= numberOfPlayers &&*/ numberOfPrizesInGoal >= numberOfPrizes && !countdownHasStarted ) {
            countdownHasStarted = true;
            endTime = Time.fixedTime + 3;
        }
        if ( countdownHasStarted && endTime - Time.fixedTime <= 0 ) {
            Application.LoadLevel( Application.loadedLevel + 1 );
        }
	}

    void OnGUI() {
        if ( /*numberOfPlayersInGoal >= numberOfPlayers &&*/ numberOfPrizesInGoal >= numberOfPrizes ) {
            GUIStyle boxStyle = GUI.skin.GetStyle( "Box" );
            boxStyle.alignment = TextAnchor.MiddleCenter;
            boxStyle.fontSize = 60;
            boxStyle.font = font;
            GUI.Box( new Rect( 0, 0, Screen.width, Screen.height ), "These worlds are\nnow at peace", boxStyle );
        }
    }

    public void playerReachedGoal() {
        numberOfPlayersInGoal++;
    }

    public void playerUnReachedGoal() {
        numberOfPlayersInGoal--;
    }

    public void ballReachedGoal( int ballId ) {
        numberOfPrizesInGoal++;
        ballInGoal[ ballId - 1 ] = true;
    }
}
