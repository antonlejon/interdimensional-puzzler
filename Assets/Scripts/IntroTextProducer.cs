﻿using UnityEngine;
using System.Collections;

public class IntroTextProducer : MonoBehaviour {

    public string[] messages;
    public float[] messagePauseTimes;
    public int[] messageFontSize;
    public float letterPauseTime = 0.1f;

	// Use this for initialization
	void Start () {
        StartCoroutine( writeText() );
	}
	
	// Update is called once per frame
	void Update () {

	}

    private IEnumerator writeText() {
        bool done = false;
        int currentPosition = 0;
        int currentMessage = 0;

        if ( messages.Length >= 1 ) {
            while ( !done ) {
                // Reset message if new
                if ( currentPosition == 0 ) {
                    GetComponent<GUIText>().text = "";
                    GetComponent<GUIText>().fontSize = messageFontSize[ currentMessage ];
                }
                // Add letters to current message
                if ( currentPosition < messages[ currentMessage ].Length ) {
                    GetComponent<GUIText>().text += messages[ currentMessage ][ currentPosition++ ];
                    yield return new WaitForSeconds( letterPauseTime );
                } else {
                    // If done
                    if ( ++currentMessage >= messages.Length ) {
                        done = true;
                    } else {
                        // Next message
                        yield return new WaitForSeconds( messagePauseTimes[ currentMessage - 1 ] );
                        currentPosition = 0;
                    }
                }
            }
        }
    }
}
