﻿using UnityEngine;
using System.Collections;

public class KeyboardControls : MonoBehaviour {

    public float fastSpeed = 1000;
    public float normalSpeed = 400;
    public float slowSpeed = 100;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Add force, but stop eventually
        /*Vector2 acceleration = new Vector2();
        acceleration.x = Input.GetAxis( "Horizontal" ) * speed;
        acceleration.y = Input.GetAxis( "Vertical" ) * speed;
        //this.gameObject.transform.position = new Vector2( this.gameObject.transform.position.x + acceleration.x, this.gameObject.transform.position.y + acceleration.y );
        this.GetComponent<Rigidbody2D>().AddForce( acceleration ); // Add force*/
        Vector2 velocity = new Vector2( Input.GetAxis( "Horizontal" ), Input.GetAxis( "Vertical" ) );
        if ( Input.GetAxis( "Fast" ) > 0 ) {
            velocity *= fastSpeed;
        } else if ( Input.GetAxis( "Slow" ) > 0 ) {
            velocity *= slowSpeed;
        } else {
            velocity *= normalSpeed;
        }
        GetComponent<Rigidbody2D>().velocity = velocity;

        if ( Input.GetKeyDown( KeyCode.R ) ) {
            Application.LoadLevel( Application.loadedLevel );
        }
	}
}
