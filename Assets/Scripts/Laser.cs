﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {

    public Vector2 initialForce = new Vector2( 0.0f, 0.0f );
    public float lifetime = 1.0f;
    private float createdAtTime;
    public GameObject explosion;

	// Use this for initialization
	void Start () {
        this.createdAtTime = Time.fixedTime;
	}
	
	// Update is called once per frame
	void Update () {
        if ( Time.fixedTime >= this.createdAtTime + lifetime ) {
            Instantiate( explosion, this.gameObject.transform.position, this.gameObject.transform.rotation );
            Destroy( this.gameObject );
        }
	}

    void OnTriggerEnter2D( Collider2D collider ) {
        if ( !collider.gameObject.CompareTag( "Player" ) ) {
            Instantiate( explosion, this.gameObject.transform.position, this.gameObject.transform.rotation );
            Destroy( this.gameObject );
        }
    }
}
