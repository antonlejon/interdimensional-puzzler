﻿using UnityEngine;
using System.Collections;

public class PlayerGoal : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter2D( Collider2D collider ) {
        if ( collider.gameObject.CompareTag( "Player" ) ) {
            PlayerScript playerScript = collider.gameObject.GetComponent( "PlayerScript" ) as PlayerScript;
            playerScript.ReachedGoal();
        }
    }
    void OnTriggerStay2D( Collider2D collider ) {
        if ( collider.gameObject.CompareTag( "Player" ) ) {
            PlayerScript playerScript = collider.gameObject.GetComponent( "PlayerScript" ) as PlayerScript;
            playerScript.ReachedGoal();
        }
    }
}
