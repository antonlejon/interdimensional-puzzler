﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScript : MonoBehaviour {

    public Camera playerCamera;
    private bool reachedGoal;
    public GameObject bulletPrefab;
    public float shotCooldown;
    private float lastShotTime;
    public Font font;
    private bool tryingToEnterGoal = false;
    public GameObject arrow;

    private List<Ball> balls;
    private List<Arrow> arrows;

    public int[] requiresBall;

	// Use this for initialization
	void Start () {
        lastShotTime = 0.0f;
        Ball[] allBalls = FindObjectsOfType<Ball>();
        balls = new List<Ball>();
        arrows = new List<Arrow>();
        foreach ( Ball ball in allBalls ) {
            if ( ball.transform.parent == transform.parent ) {
                balls.Add( ball );
                GameObject arrowObject = Instantiate( arrow );
                Arrow arrowComponent = arrowObject.GetComponent<Arrow>();
                arrowComponent.player = this;
                arrowComponent.ball = ball;
                arrowObject.transform.position = transform.position;
                arrowObject.name = "Arrow " + ball.name;
                arrows.Add( arrowComponent );
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        /*if ( Input.GetButton( "Fire1" ) ) {
            if ( Time.fixedTime >= lastShotTime + shotCooldown ) {
                lastShotTime = Time.fixedTime;
                //for ( int i = 0; i < 8; i++ ) {
                    Instantiate( bulletPrefab, this.gameObject.transform.position, this.gameObject.transform.rotation );
                //}
            }
        }*/
        foreach ( Ball ball in balls ) {
            if ( !ball.inGoal ) {
                Debug.DrawRay( transform.position, ball.transform.position - transform.position, Color.white );
            }
        }
	}

    void OnGUI() {
        GUIStyle boxStyle = GUI.skin.GetStyle( "Box" );
        boxStyle.alignment = TextAnchor.MiddleCenter;
        boxStyle.fontSize = 20;
        boxStyle.font = font;
        GUI.backgroundColor = new Color( 0.0f, 0.0f, 0.0f, 1.0f );

        if ( reachedGoal ) {
            GlobalVariables globalVariables = GameObject.Find( "Overseer" ).GetComponent( "GlobalVariables" ) as GlobalVariables;
            if ( globalVariables.numberOfPlayersInGoal != globalVariables.numberOfPlayers ) {
                GUI.Box( new Rect( playerCamera.GetComponent<Camera>().pixelRect.x, playerCamera.GetComponent<Camera>().pixelRect.y, playerCamera.GetComponent<Camera>().pixelRect.width, playerCamera.GetComponent<Camera>().pixelRect.height ), "This world is saved", boxStyle );
            }
        } else {
            if ( tryingToEnterGoal ) {
                GUI.Box( new Rect( playerCamera.GetComponent<Camera>().pixelRect.x, playerCamera.GetComponent<Camera>().pixelRect.y, playerCamera.GetComponent<Camera>().pixelRect.width, playerCamera.GetComponent<Camera>().pixelRect.height ), "More souls need saving", boxStyle );
            }
        }
        

    }

    void OnTriggerEnter2D( Collider2D collider ) {
        if ( collider.gameObject.CompareTag( "PlayerGoal" ) ) {
            tryingToEnterGoal = true;
        }
    }
    void OnTriggerExit2D( Collider2D collider ) {
        if ( collider.gameObject.CompareTag( "PlayerGoal" ) ) {
            tryingToEnterGoal = false;
        }
    }

    public void ReachedGoal() {
        GlobalVariables globalVariables = GameObject.Find( "Overseer" ).GetComponent( "GlobalVariables" ) as GlobalVariables;
        bool requiredBallsInGoal = true;
        foreach ( int ballId in requiresBall ) {
            if ( !globalVariables.ballInGoal[ ballId - 1 ] ) {
                requiredBallsInGoal = false;
            }
        }
        if ( requiredBallsInGoal ) {
            // Reached goal
            if ( !this.reachedGoal ) {
                this.reachedGoal = true;
                //playerCamera.enabled = false;
                globalVariables.playerReachedGoal();
                this.gameObject.GetComponent<Collider2D>().enabled = false;
            } else {
                this.reachedGoal = false;
                //playerCamera.enabled = false;
                globalVariables.playerUnReachedGoal();
                this.gameObject.GetComponent<Collider2D>().enabled = true;
            }
        }
    }
}
