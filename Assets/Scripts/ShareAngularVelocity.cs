﻿using UnityEngine;
using System.Collections;

public class ShareAngularVelocity : MonoBehaviour {

    public string objectTag = "Player";

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {
	
	}

    /*void LateUpdate() {
        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        foreach ( GameObject obj in objects ) {
            if ( obj.CompareTag( "Player" ) ) {
                if ( obj != this.gameObject ) {
                    // This is another player object - apply the same force to it
                    //obj.rigidbody2D.rotation = this.gameObject.rigidbody2D.rotation;
                }
            }
        }
    }*/

    void OnCollisionStay2D( Collision2D collision ) {
        this.ApplyAngularVelocityToOthers();
    }
    void OnCollisionEnter2D( Collision2D collision ) {
        this.ApplyAngularVelocityToOthers();
    }

    void ApplyAngularVelocityToOthers() {
        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        foreach ( GameObject obj in objects ) {
            if ( obj.CompareTag( objectTag ) ) {
                if ( obj != this.gameObject ) {
                    // This is another player object - apply the same force to it
                    //obj.rigidbody2D.rotation = this.gameObject.rigidbody2D.rotation;
                    obj.GetComponent<Rigidbody2D>().angularVelocity = this.gameObject.GetComponent<Rigidbody2D>().angularVelocity;
                }
            }
        }
    }
}
