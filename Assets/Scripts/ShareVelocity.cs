﻿using UnityEngine;
using System.Collections;

public class ShareVelocity : MonoBehaviour {

    public string objectTag = "Player";
    private Vector3 previousPosition;
    private Vector3 startPosition;
    private Vector3 relativePosition;
    private bool stuck;
    public bool constantlyStuckForCinematics = false;

	// Use this for initialization
	void Start() {
        startPosition = this.gameObject.transform.position;
        previousPosition = startPosition;
        relativePosition = Vector3.zero;
	}
    	
	// Update is called once per frame
	void Update() {
        relativePosition = this.gameObject.transform.position - startPosition;

        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        if ( stuck || constantlyStuckForCinematics ) {
            foreach ( GameObject obj in objects ) {
                if ( obj.CompareTag( objectTag ) ) {
                    if ( obj != this.gameObject ) {
                        ShareVelocity shareVelocity = obj.GetComponent( "ShareVelocity" ) as ShareVelocity;
                        obj.transform.position = shareVelocity.startPosition + relativePosition;
                        obj.GetComponent<Rigidbody2D>().velocity = this.gameObject.GetComponent<Rigidbody2D>().velocity;
                        obj.GetComponent<Rigidbody2D>().rotation = this.gameObject.GetComponent<Rigidbody2D>().rotation;
                        obj.GetComponent<Rigidbody2D>().angularVelocity = this.gameObject.GetComponent<Rigidbody2D>().angularVelocity;
                    }
                }
            }
        }
	}

    void LateUpdate() {
        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        if ( stuck ) {
            foreach ( GameObject obj in objects ) {
                if ( obj.CompareTag( objectTag ) ) {
                    if ( obj != this.gameObject ) {
                        ShareVelocity shareVelocity = obj.GetComponent( "ShareVelocity" ) as ShareVelocity;
                        obj.transform.position = shareVelocity.startPosition + relativePosition;
                        obj.GetComponent<Rigidbody2D>().velocity = this.gameObject.GetComponent<Rigidbody2D>().velocity;
                        obj.GetComponent<Rigidbody2D>().rotation = this.gameObject.GetComponent<Rigidbody2D>().rotation;
                        obj.GetComponent<Rigidbody2D>().angularVelocity = this.gameObject.GetComponent<Rigidbody2D>().angularVelocity;
                    }
                }
            }
        }

        previousPosition = this.gameObject.transform.position;
    }
    
    void OnCollisionStay2D( Collision2D collision ) {
        if ( collision.gameObject.CompareTag( "Player" ) ) {
            this.ApplyVelocityToOthers();
        } else {
            stuck = true;
        }
    }
    void OnCollisionEnter2D( Collision2D collision ) {
        this.ApplyVelocityToOthers();
        if ( !collision.gameObject.CompareTag( "Player" ) ) {
            stuck = true;
        }
    }
    void OnCollisionExit2D( Collision2D collision ) {
        if ( !collision.gameObject.CompareTag( "Player" ) ) {
            stuck = false;
        }
    }

    void ApplyVelocityToOthers() {
        GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        foreach ( GameObject obj in objects ) {
            if ( obj.CompareTag( objectTag ) ) {
                if ( obj != this.gameObject ) {
                    obj.GetComponent<Rigidbody2D>().velocity = this.gameObject.GetComponent<Rigidbody2D>().velocity;
                }
            }
        }
    }
}
