﻿using UnityEngine;
using System.Collections;

public class StopMusic : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    GameObject[] objects = FindObjectsOfType( this.gameObject.GetType() ) as GameObject[];
        foreach ( GameObject obj in objects ) {
            if ( obj.GetComponent( "KeepMusicPlaying" ) as KeepMusicPlaying != null ) {
                if ( obj != this.gameObject ) {
                    Destroy( obj );
                }
            }
        }
        Destroy( this );
	}
}